from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.conf import settings
from core.views import *
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^admin/', include('loginas.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),

    url(r"^accounts/", include("account.urls")),
    url(r"^p/", include("codes.urls")),
    url(r"^$", home, name="home"),
    url(r"^api/$", api_create, name="api"),
    url(r"^a/(?P<code>[a-zA-Z0-9]+)/$", create_by_url, name="create_by_url"),
    # acortadores
    url(r"^i/(?P<code>[a-zA-Z0-9]+)$", redir_in, name="redir_in"),
    url(r"^r/(?P<code>[a-zA-Z0-9\-]+)$", redir_out, name="redir_out"),

    # admin 
    url(r"^services/url/create$", shorturl_create, name="create_url"),
    url(r"^services/url/delete/(?P<pk>\d+)$", delete_url, name="delete_url"),
    url(r"^services/url$", list_shorturl, name="list_url"),
    url(r"^services/url/d/$", shorturl_byday, name="list_url_byday"),
    url(r"^services/url/(?P<pk>\d+)$", detail_url, name="detail_url"),
    url(r"^services/create$", create_service, name="create_service"),
    url(r"^services/delete/domain/$", delete_urls_from_domain, name="delete_domain"),
    url(r"^services/save/(?P<pk>\d+)$", save_service, name="save_service"),
    url(r"^services/delete/(?P<pk>\d+)$", delete_service, name="delete_service"),
    url(r"^services/delete/all/(?P<pk>\d+)$", delete_service_urls, name="delete_service_urls"),
    url(r"^services$", list_service, name="list_service"),

      url(r'^services/a/$', analitic_detail, name='analitic_detail'),
      url(r'^services/a/create/$', analitic_create, name='analitic_create'),
      url(r'^services/a/update/$', analitic_update, name='analitic_update'),

    url(r'^django-rq/', include('django_rq.urls')),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
 + staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [
        url(r'^rosetta/', include('rosetta.urls'))
    ]
