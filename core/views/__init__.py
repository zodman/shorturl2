#!/usr/bin/env python
# encoding=utf-8
# made by zodman
from django.shortcuts import render, get_object_or_404
from django.views.generic import CreateView, ListView, DeleteView, RedirectView, TemplateView
from django.views.generic import DetailView
from ..models import Service, ShortUrl, Visit
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.db import IntegrityError
from django.utils.translation import ugettext as _
from django.urls import reverse_lazy as reverse
from ..baseconv import base32
from ipware.ip import get_real_ip
from django.core.cache import cache
from django.contrib.auth import authenticate
import json
from django.db import models
from django.views.decorators.csrf import csrf_exempt
from google_measurement_protocol import Event, report
import uuid
from .analytics import  analitic_create, analitic_detail, analitic_update
from .internal import *
from .external import *
__all__ = [
        "create_service",
        "save_service",
        "list_service",
        "shorturl_create",
        "list_shorturl",
        "delete_url",
        "delete_service",
        "delete_service_urls",
        "redir_in",
        "redir_out",
        "detail_url",
        "api_create",
        "home",
        'analitic_create', 'analitic_detail','analitic_update',
        'shorturl_byday',
        'create_by_url',
        'delete_urls_from_domain'
]

