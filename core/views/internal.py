#!/usr/bin/env python
# encoding=utf-8
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# made by zodman
from django.shortcuts import render, get_object_or_404
from django.views.generic import CreateView, ListView, DeleteView, RedirectView, TemplateView
from django.views.generic import DetailView
from ..models import Service, ShortUrl, Visit
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.contrib.sites.models import Site
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.db import IntegrityError
from django.db.models import Q
from django.utils.translation import ugettext as _
from django.urls import reverse_lazy as reverse
from ..baseconv import base32
from ipware.ip import get_real_ip
from django.core.cache import cache
from django.contrib.auth import authenticate
import json
from django.db import models
from django.views.decorators.csrf import csrf_exempt
from google_measurement_protocol import Event, report
import uuid
from .analytics import analitic_create, analitic_detail, analitic_update
from codes.views import CodeRedirectMixin
from django import forms
from django.utils import timezone
import datetime
from django.db.models import Count
from django.conf import settings


class HomeView(LoginRequiredMixin, CodeRedirectMixin, TemplateView):
    template_name = "core/home/index.html"
    index = 15
    colors = [
        'rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(255, 205, 86)',
        'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)',
        'rgb(201, 203, 207)'
    ]

    def get_context_data(self, **kwargs):
        cdata = super(HomeView, self).get_context_data(**kwargs)
        account = self.request.user.account
        qs = Visit.objects.filter(service__account=account)
        extra = {
            'day': 'DATE(core_visit.visit_date)',
            'domain':"""
                    SUBSTRING_INDEX(
                        SUBSTRING_INDEX(
                            SUBSTRING_INDEX(
                                SUBSTRING_INDEX(core_service.domain, '/', 3), 
                            '://', -1), 
                        '/', 1), 
                    '?', 1)
            """
        }
        if settings.TEST:
            extra['domain']= extra['domain'].replace("SUBSTRING_INDEX", 'SUBSTR')
        qs = qs.extra(extra)
        qs = qs.values('day', 'domain')
        qs = qs.annotate(available=models.Count('visit_date'))
        v = qs.values('day','domain','available').order_by('domain','day')
        d  = {}
        for i in v:
            d.setdefault(i['domain'],[])
            d[i['domain']].append(i['available'])
        r = {
            'labels': list(qs.values_list('day', flat=True)),
            'data': d
        }
        cdata["graph"]= r
        cdata['list'] = v
        cdata['colors']= self.colors
        cdata['service_exists'] = self.check_service()
        return cdata

    def check_service(self):
        # check if user had services to show intro or not
        services = Service.objects.filter(
            account=self.request.user.account, limit__isnull=True)
        return services.exists()


home = HomeView.as_view()


class CreateService(LoginRequiredMixin, CodeRedirectMixin, CreateView):
    model = Service
    fields = ("domain", )

    def get_success_url(self):
        return reverse("list_service")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.account = self.request.user.account
        try:
            self.object.save()
        except IntegrityError:
            messages.error(
                self.request,
                _("Error, Ya existe un servicio {}".format(
                    self.object.domain)))
        return HttpResponseRedirect(self.get_success_url())


create_service = CreateService.as_view()


class SaveService(LoginRequiredMixin, CodeRedirectMixin, TemplateView):
    def post(self, request, *args, **kwargs):
        id = kwargs.get("pk")
        service = Service.objects.get(id=id, account=request.user.account)
        limit = request.POST.get("limit", -1)
        if limit == "" or int(limit) < 0:
            limit = None
        service.limit = limit
        service.save()
        messages.info(request, "limit setted")
        return HttpResponseRedirect(reverse("list_service"))


save_service = SaveService.as_view()


class ListService(LoginRequiredMixin, CodeRedirectMixin, ListView):
    model = Service
    template_name = "core/service_list.html"

    def get_queryset(self):
        qs = super(ListService, self).get_queryset()
        account = self.request.user.account
        res = qs.filter(account__id=account.id)
        return res

    def get_context_data(self, **kwargs):
        context = super(ListService, self).get_context_data(**kwargs)
        return context


class ListShortURL(LoginRequiredMixin, CodeRedirectMixin, ListView):
    model = ShortUrl
    paginate_by = 30
    ordering = ("-id", )

    def get_queryset(self):
        qs = super(ListShortURL, self).get_queryset()
        account = self.request.user.account
        account_qs = qs.filter(
            Q(service__account=account) | Q(account=account))
        qs_result = account_qs.exclude(out__isnull=True)
        q = self.request.GET.get("q")
        if q:
            qs_result = qs_result.filter(
                Q(out__contains=q) | Q(url__contains=q))
        return qs_result


list_service = ListService.as_view()
list_shorturl = ListShortURL.as_view()


class CreateShorUrl(LoginRequiredMixin, CodeRedirectMixin, CreateView):
    model = ShortUrl
    fields = ("url", )

    def get_success_url(self):
        return reverse("list_url")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        account = self.request.user.account
        try:
            self.object.generate_url(account)
            messages.info(self.request,
                          _("Url %s was created ") % self.object.out)
        except IntegrityError:
            messages.error(self.request, _("Error, Ya existe esa url"))
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(CreateShorUrl, self).get_context_data(**kwargs)
        context["site"] = Site.objects.get_current()
        user_pk = self.request.user.pk
        code = base32.from_decimal(user_pk)
        context["code"] = code
        services = Service.objects.filter(
            account=self.request.user.account, limit__isnull=True)
        context["service_exists"] = services.exists()
        return context


shorturl_create = CreateShorUrl.as_view()


class DeleteUrl(LoginRequiredMixin, CodeRedirectMixin, DeleteView):
    model = ShortUrl
    success_url = reverse('list_url')

    def get_queryset(self):
        qs = super(DeleteUrl, self).get_queryset()
        return qs.filter(account=self.request.user.account)


delete_url = DeleteUrl.as_view()



def delete_urls(account_id, service_id):
    urls = ShortUrl.objects.filter(
        service__account__id=account_id, service__id=service_id)
    urls.delete()

import django_rq

class DeleteServiceUrls(LoginRequiredMixin, CodeRedirectMixin, DeleteView):
    model = Service
    success_url = reverse('list_service')

    def delete(self, request, *args, **kwargs):
        obj = self.get_object()

        django_rq.enqueue(delete_urls, request.user.account.id, obj.id)
        # urls = ShortUrl.objects.filter(
            # service__account=request.user.account, service=obj)
        # urls.delete()
        messages.info(request, "deleted!")
        return super(DeleteServiceUrls, self).delete(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DeleteServiceUrls, self).get_context_data(**kwargs)
        obj = self.get_object()
        urls = ShortUrl.objects.filter(
            service__account=self.request.user.account, service=obj)
        context["urls"] = urls
        return context


delete_service_urls = DeleteServiceUrls.as_view()


class DeleteService(DeleteServiceUrls):
    def delete(self, request, *args, **kwargs):
        messages.info(request, "deleted only service!")
        return super(DeleteServiceUrls, self).delete(request, *args, **kwargs)


delete_service = DeleteService.as_view()


class UrlDetail(DetailView, CodeRedirectMixin, LoginRequiredMixin):
    model = ShortUrl

    def get_queryset(self):
        qs = super(UrlDetail, self).get_queryset()
        account = self.request.user.account
        return qs.filter(Q(account=account) | Q(service__account=account))


detail_url = UrlDetail.as_view()


class ListByDay(LoginRequiredMixin, CodeRedirectMixin, ListView):
    ordering = ("-created_date", )
    model = Visit
    template_name = "core/shorturl_day_list.html"

    def get_context_data(self, **kwargs):
        data = super(ListByDay, self).get_context_data(**kwargs)
        d = self.request.GET.get("date")
        domain = self.request.GET.get("domain")
        qs = self.get_queryset()
        qs.update()
        total = qs.count()
        q = qs.values("country").distinct().annotate(
            ips_country=models.Count("country"))
        data["countries"] = q.order_by("-ips_country")
        data["ddate"] = d
        data["total"] = total
        data["domain"] = domain
        return data

    def get_queryset(self):
        qs = super(ListByDay, self).get_queryset()
        account = self.request.user.account
        d = self.request.GET.get("date")
        domain = self.request.GET.get("domain")
        params = {}
        if domain:
            qs = qs.filter(shorturl__service__domain__icontains=domain)

        qs = qs.filter(Q(service__account=account)|Q(shorturl__service__account=account))
        if d:
            params.update({'visit_date__date': d})
        account_qs = qs.filter(**params)
        return account_qs


shorturl_byday = ListByDay.as_view()


def delete_urls_from_domain(request):
    class DomainForm(forms.Form):
        domain = forms.CharField()

        def delete_domain(self, request):
            domain = self.cleaned_data.get("domain")
            r = ShortUrl.objects.filter(
                account=request.user.account, out__icontains=domain).delete()
            return r

    domain = request.GET.get("domain")
    if request.method == "GET":
        form = DomainForm()

    if request.method == "POST":
        form = DomainForm(request.POST)
        if form.is_valid():
            resp = form.delete_domain(request)
            messages.info(request, "delete urls {}".format(resp))
        else:
            messages.error(requesr, "error")

    context = {'domain': domain, 'form': form}
    return render(request, "core/delete_urls.html", context)
