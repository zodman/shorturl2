#!/usr/bin/env python
# encoding=utf-8
# made by zodman
from django.shortcuts import render, get_object_or_404
from django.views.generic import CreateView, ListView, DeleteView, RedirectView, TemplateView
from django.views.generic import DetailView
from ..models import Service, ShortUrl, Visit, Analitic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.db import IntegrityError
from django.utils.translation import ugettext as _
from django.urls import reverse_lazy as reverse
from ..baseconv import base32, BaseConverter
from ipware.ip import get_real_ip
from django.core.cache import cache
from django.contrib.auth import authenticate
import json
from django.db import models
from django.views.decorators.csrf import csrf_exempt
from google_measurement_protocol import Event, report
import uuid
from .analytics import analitic_create, analitic_detail, analitic_update
from memorised.decorators import memorise
from django.contrib.auth.models import User


class RedirMix(object):
    def get_client_ip(self, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip

    def get_ip(self):
        ip = get_real_ip(self.request)
        if not ip:
            return self.get_client_ip(self.request)
        return ip

    def _get_key(self, hash_str):
        ip = self.get_ip()
        key = "{}:{}".format(ip, hash_str)
        return key

    def check_in(self, hash_str):
        key = self._get_key(hash_str)
        return cache.get(key, False)

    def set_in(self, hash_str):
        key = self._get_key(hash_str)
        return cache.set(key, True)

    def reset_in(self, hash_str):
        key = self._get_key(hash_str)
        return cache.set(key, None)

    def register_analytics(self, shorturl_obj, request=None):
        client_id = uuid.uuid4()
        domain = shorturl_obj.get_domain()
        headers = {}
        if request:
            headers.update({
                'User-Agent':
                request.META.get('HTTP_USER_AGENT', "empty")
            })
        try:
            au_code = shorturl_obj.account.analitic.au
            evnt = Event("visit", "shorturl", label=domain)
            return report(au_code, client_id, evnt, extra_headers=headers)
        except Analitic.DoesNotExist:
            pass


class RedirIN(RedirectView, RedirMix):
    def get_redirect_url(self, *args, **kwargs):
        if "code" in kwargs:
            code = kwargs.get("code")
            id = base32.to_decimal(code)
            shorturl = get_object_or_404(ShortUrl, id=id)
            shorturl.random_short()
            self.set_in(shorturl.uuid)
            return shorturl.out
        else:  # pragma: no cover
            raise Http404, "no encontrado"


redir_in = RedirIN.as_view()


class RedirOUT(RedirectView, RedirMix):
    def get_redirect_url(self, *args, **kwargs):
        if "code" in kwargs:
            code = kwargs.get("code")
            shorturl = get_object_or_404(ShortUrl, uuid=code)
            shorturl.random_short()
            if not self.check_in(shorturl.uuid):
                return shorturl.get_in()
            self.reset_in(code)
            ip = self.get_ip()
            shorturl.decrement_visit()
            visit = Visit(service=shorturl.service, ip=ip, shorturl=shorturl)
            visit.save()
            self.register_analytics(shorturl, self.request)
            return shorturl.url
        else:  # pragma: no cover
            raise Http404, "frfrfr no encontrado"


redir_out = RedirOUT.as_view()
@memorise(ttl=60*10)
def _create_url(url, account):

    obj = ShortUrl(url=url)
    obj.random_short(account)
    if obj.pk:
        url = obj.get_in()
        return url
    else:  # pragma: no cover
        u = ShortUrl.objects.get(url=url, account=account)
        u.random_short(account)
        return u.get_in()


@csrf_exempt
def api_create(request):
    username = request.POST.get("username")
    p = request.POST.get("password")
    url = request.POST.get("url")
    user = authenticate(username=username, password=p)
    account = user.account
    if user:
        url = _create_url(url, account)
        d = {'url': url}
    else:  # pragma: no cover
        d = {"error": "no auth"}
    return HttpResponse(json.dumps(d))


@csrf_exempt
def create_by_url(request, code):
    url = request.GET.get("url")
    user_pk = base32.to_decimal(code)
    user = User.objects.get(pk=user_pk)

    if user:
        account = user.account
        try:
            url = _create_url(url, account)
        except Exception as e:  # pragma: no cover
            return HttpResponse("{}".format(e), status=409)
    return HttpResponseRedirect(url)
