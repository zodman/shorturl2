from django.views.generic import DetailView, ListView, UpdateView, CreateView
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.db import IntegrityError
from ..models import Analitic
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.utils.translation import ugettext_lazy  as _
from django.urls import reverse_lazy as reverse
from django.contrib.auth.mixins import LoginRequiredMixin


class AnalyticMixin(LoginRequiredMixin):
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.account = self.request.user.account
        try:
            self.object.save()
        except IntegrityError:
            messages.error(self.request, _("Error, Ya existe una cuenta"))
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("analitic_detail")
    def get_object(self, queryset=None):
        account = self.request.user.account
        try:
            return Analitic.objects.get(account=account)
        except Analitic.DoesNotExist:
            return Analitic.objects.none()



class AnaliticCreateView(AnalyticMixin, CreateView):
    model = Analitic
    fields = ("au",)


analitic_create = AnaliticCreateView.as_view()


class AnaliticDetailView(AnalyticMixin, DetailView):
    model = Analitic


analitic_detail = AnaliticDetailView.as_view()

class AnaliticUpdateView(AnalyticMixin, UpdateView, ):
    model = Analitic
    fields = ("au",)

analitic_update = AnaliticUpdateView.as_view()

