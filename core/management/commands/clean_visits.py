from django.core.management.base import BaseCommand, CommandError
from core.models import Visit
from django.utils import timezone
from datetime import timedelta


class Command(BaseCommand):
    help = 'delete 30 days ago visits'

    
    def handle(self, *args, **options):
        last_month = timezone.now() - timedelta(days=30)
        results = Visit.objects.filter(created_date__lte=last_month).delete()
        print "borrado {}".format(results)
        