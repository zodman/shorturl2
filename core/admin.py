from django.contrib import admin
from .models import Service, ShortUrl, Visit, Analitic

admin.site.register(Service)

class UrlAdmin(admin.ModelAdmin):
    search_fields = ("out","url")

admin.site.register(ShortUrl, UrlAdmin)

class VisitAdmin(admin.ModelAdmin):
    list_display=("ip","visit_date","country")

admin.site.register(Visit, VisitAdmin)

admin.site.register(Analitic)
