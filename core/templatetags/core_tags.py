from django import template
import pycountry
register = template.Library()

@register.filter
def country(value):
    try:
        return pycountry.countries.get(name=value).alpha_2.lower()
    except KeyError:
        return ""


@register.simple_tag
def create_short(*args):
    shorturl_object, service_object = args
    return shorturl_object.random_short(service_object)
