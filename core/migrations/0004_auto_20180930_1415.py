# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-09-30 14:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20171021_0943'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='visit',
            name='url',
        ),
        migrations.AddField(
            model_name='visit',
            name='service',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='visits', to='core.Service'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='visit',
            name='shorturl',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='visits', to='core.ShortUrl'),
        ),
    ]
