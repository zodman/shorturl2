from test_plus.test import TestCase
from django.urls import reverse
from core.models import ShortUrl, Service, Visit
import requests
from django.contrib.auth.models import User
import mock
from codes.models import AccountCode
from django import test
import os
from autofixture import AutoFixture
from baseconv import base32 

class TestCreateService(TestCase):
    username = 'zodman'
    domain = "mycustom.com"
    url = "http://google.com"

    def setUp(self):
        super(TestCreateService, self).setUp()
        self.user = self.make_user(self.username)
        self.data = {
            'domain': "http://{}{}".format(self.domain, "?url={}"),
        }

    
    def test_create_url_user(self):
        user_id = self.user.id
        self.limit_create_service(limit=-1)
        user_code=base32.from_decimal(user_id)
        resp = self.get('create_by_url', code= user_code,
                         data={'url': self.url})
        self.response_302(resp)
    
    def generate_visit(self, shorturl):
        code = shorturl._get_key()
        self.get("redir_in", code=code)
        self.response_302()
        os.environ["REMOTE_ADDR"] = "127.0.0.1"
        self.get("redir_out", code=shorturl.uuid)
        self.response_302()

    def test_create_url(self):
        self.create_service()
        self.limit_create_service(limit=10)
        shorturl = self.create_url()
        limit = shorturl.service.limit
        self.generate_visit(shorturl)
        service = Service.objects.get(id=shorturl.service.id)
        # decrement
        self.assertEqual(limit-1, service.limit)
        self.assertEqual(service.visits.count(), 1)

    def test_api(self):
        self.create_service()
        data = {
            'username': self.username,
            'password': 'password',
            'url': self.url,
        }
        self.post("api", data=data)
        self.response_200()

    def create_url(self):
        url = self.url
        service = self.create_service()
        self.assertTrue(service)

        with self.login(username=self.username):
            self.get('create_url')
            data = {
                'url': url,
            }
            self.post('create_url', data=data)
            self.response_302()
        return ShortUrl.objects.get(url=url)

    def test_detail_url(self):
        shorturl = self.create_url()
        self.generate_visit(shorturl)
        with self.login(username=self.username):
            self.get_check_200('detail_url', pk=shorturl.id)
            
    def test_delete_service_urls(self):
        service = self.create_service()
        with self.login(username=self.username):
            resp = self.get('delete_service_urls', pk=service.id)
            self.response_200(resp)
            resp = self.post('delete_service_urls', pk=service.id)
            self.response_302(resp)
            e = Service.objects.filter(id=service.id).exists()
            self.assertFalse(e)

    def test_delete_service(self):
        service = self.create_service()
        with self.login(username=self.username):
            resp = self.get('delete_service', pk=service.id)
            self.response_200(resp)
            resp = self.post('delete_service', pk=service.id)
            self.response_302(resp)
            e = Service.objects.filter(id=service.id).exists()
            self.assertFalse(e)

    def test_delete_url(self):
        url = self.create_url()

        with self.login(username=self.username):
            resp = self.post('delete_url', pk=url.id)
            self.response_302(resp)
            exists = ShortUrl.objects.filter(id=url.id).exists()
            self.assertFalse(exists)

    def test_create_wrong(self):
        self.assertTrue(User.objects.get(username=self.username).account)
        with self.login(username=self.username):
            data = {
                'domain': self.domain,
            }
            resp = self.post('create_service', data=data)
            self.response_200()
            form = self.get_context("form")
            self.assertTrue(form.errors)
            self.assertResponseContains(
                "url doesnt contains {}", response=resp)

    def test_create_service(self):
        self.limit_create_service()
        self.limit_create_service(limit=-1)

    def limit_create_service(self, limit=100):
        service = self.create_service()
        with self.login(username=self.username):
            data = {'limit': limit}
            self.post("save_service", pk=service.id, data=data)
            self.response_302()
        s = Service.objects.get(id=service.id)
        if limit == -1:
            limit = None
        self.assertEqual(s.limit, limit)

    def create_service(self):
        services = Service.objects.filter(domain__contains=self.domain)
        if not services.exists():
            with self.login(username=self.username):
                self.post('create_service', data=self.data)
                self.response_302()
            service = (Service.objects
                       .filter(domain__contains=self.domain).first())
            return service
        else:
            return services.first()

    def test_delete_domains(self):
        self.create_service()
        url = self.create_url()
        with self.login(username=self.username):
            self.get_check_200("delete_domain")
            self.post('delete_domain', data={'domain': self.domain})
            e = ShortUrl.objects.filter(id=url.id).exists()
            self.assertFalse(e)

    def test_url_by_day(self):
        self.create_service()
        self.create_url()
        with self.login(username=self.username):
            self.get_check_200('list_url_byday')
            self.get_check_200('list_url_byday', data={'domain':self.domain})

class ViewTests(TestCase):
    def setUp(self):
        super(ViewTests, self).setUp()
        self.user = self.make_user("zodman")
        fixServ = AutoFixture(Service)
        fixServ.create(10)
        (Service.objects.all()
                .update(account=self.user.account))
        AutoFixture(ShortUrl).create(100)
    def test_analytics(self):
        with self.login(username=self.user):
            self.get_check_200('analitic_detail')
            self.get_check_200('analitic_create')
            self.post('analitic_create', data={'au': 'AAAAA'})
            self.get_check_200('analitic_detail')

    def test_list_service(self):
        with self.login(self.user):
            self.get_check_200("list_service")
            self.get_check_200("list_url")
            self.get_check_200("home")

