# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 background=dark
from __future__ import unicode_literals
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.sites.models import Site
from django.db import models
from account.models import Account
import requests
import urllib
import uuid
from baseconv import base32 as base20
import re
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils import timezone
import pytz
from django.conf import settings
import pygeoip
from urlparse import urlparse
from django.core.exceptions import ValidationError
from  django.core.validators  import URLValidator
from django.db import IntegrityError
import pycountry
import logging
from django.urls import reverse_lazy
import tldextract


logger = logging.getLogger(__name__)


class Analitic(models.Model):
    account = models.OneToOneField(Account)
    au = models.CharField(max_length=200, help_text="analytics AU-XXXXX-XX")

    def __unicode__(self):
        return self.au

def contains_string(value):
    if not "{}" in value:
        raise ValidationError("url doesnt contains {}")
    if not "http" in value:
        raise ValidationError("url doesnt contains http or https")
    if not "://" in value:
        raise ValidationError("url invalid")

class Service(models.Model):
    account = models.ForeignKey(Account)
    domain = models.CharField(max_length=255, 
            validators=[contains_string], unique=True,
            help_text=("URL for the short url example:"
                " http://shink.in/id/?url={}"))
    limit = models.PositiveIntegerField(null=True, blank=True, default=None)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u"{} - {}".format(self.account, self.domain)

    def get_domain(self):
        resp = tldextract.extract(self.domain.replace('{}',''))
        return resp.registered_domain

    def get_url(self,  uuid):
        site = Site.objects.get_current()
        url = "http://{}/r/{}".format(site.domain, uuid)
        return self.get_std_url(url)

    def get_std_url(self, url):
        url_api = self.domain
        #url = url_api.format(urllib.quote(url))
        url = url_api.format(url)
        res = requests.get(url).url
        return res

    def count_visits(self):
        count_visits  = Visit.objects.filter(service=self).count()
        return count_visits


class ShortUrl(models.Model):
    url = models.URLField(unique=True)
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    account = models.ForeignKey(Account, related_name="urls")
    service = models.ForeignKey("Service", related_name="urls", 
                    on_delete=models.SET_NULL, null=True)
    out = models.URLField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ("url", "account")

    def __unicode__(self):
        return u"{}".format(self.url)

    def _get_key(self):
        key = base20.from_decimal(self.pk)
        return key

    def get_in(self):
        key = self._get_key()
        site = Site.objects.get_current()
        url = reverse_lazy("redir_in", kwargs={'code':key})
        return u"http://{}/{}".format(site.domain, url)

    def get_out(self):
        uuid = self.uuid
        site = Site.objects.get_current()
        url = "http://{}/r/{}".format(site.domain, uuid)
        return url

    def decrement_visit(self):
        if self.service and not self.service.limit is None and self.service  > 0:
            if self.service.limit > 0:
                self.service.limit -=1
                self.service.save()
        return self.service.limit

    def get_url_domain(self):
        if self.out:
            r  =tldextract.extract(self.url)
            return r.registered_domain


    def get_service(self, account):
        """
        Primero obtiene el ultimo servicio que no es infinito
        Si no toma un random de los infinitos.
        """
        services = Service.objects.filter(account=account).order_by("limit")
        service_limited = services.filter(limit__isnull=False, limit__gt=0)
        if service_limited.exists():
            service = service_limited.last()
        else:
            service_infinity = services.filter(limit__isnull=True)
            service = service_infinity.order_by("?").last()
        if not service:
            service = services.last()
            assert not service is None, "no service"
            service.limit = None
            service.save()
        return service

    def generate_url(self, account):
        return self.random_short(account)

    def random_short(self, account=None):
        if account is None and self.account:
            account = self.account
            if not self.service:
                self.out = None
        self.account = account
        service = self.get_service(account)
        return self.short(service)

    def short(self, service_obj):
        if self.out is None:
            url = service_obj.get_url(self.uuid)
            self.out = url
            self.service = service_obj
            try:
                self.save()
            except IntegrityError:
                account = service_obj.account
                short=ShortUrl.objects.get(url=self.url,
                        account=account)
                return short.out
        else:
            url = self.out
        return url

    def count_visits(self):
        return self.visits.count()

    def get_visits(self):
        if self.visits.exists():
            return self.visits.extra(select={'day': 'date( created_date )'} \
                                ).values('day') \
                               .annotate(available=models.Count('created_date'))
    def get_domain(self):
        return urlparse(self.out).hostname

class Visit(models.Model):
    service = models.ForeignKey(Service, related_name="visits", 
                            on_delete=models.CASCADE)
    ip = models.GenericIPAddressField(protocol="IPv4")
    shorturl = models.ForeignKey(ShortUrl, related_name='visits',
                                 on_delete=models.SET_NULL, null=True, blank=True,
                                )
    country = models.CharField(max_length=100, null=True, blank=True)
    visit_date = models.DateTimeField(auto_now_add=True)
    created_date = models.DateTimeField(auto_now_add=True, editable=False)
    modified_date = models.DateTimeField(auto_now=True)

    @property
    def getcountry(self):
        a = pycountry.countries.get(name=self.country).alpha_2
        return a

    @property
    def get_timezone(self):
        return 'UTC'

    def __unicode__(self):
        return u"{}".format(self.ip)



@receiver(pre_save, sender=Visit)
def save_timezone(sender, **kwargs):
    instance = kwargs.get("instance")
    if not instance.country or instance.country=="":
        gi = pygeoip.GeoIP("GeoIP.dat")
        ip = instance.ip
        data = gi.country_name_by_addr(ip)
        instance.country = data
