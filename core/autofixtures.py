from .models import ShortURL, Visit
from autofixture import generators, register, AutoFixture


class VisitFixture(AutoFixture):
    field_values = {
        'name': generators.StaticGenerator('this_is_my_static_name'),
    }



#register(Visit, VisitFixture)