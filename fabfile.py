from fabric import task
from invoke import run as local

@task
def compile(c):
    local('yarn run build')
    local("git  -c user.name='Paul Draper' -c user.email='my@email.org' commit -am 'compile'")
    local("git push")

@task
def deploy(c):
    with c.prefix("cd shorturl2 && source ~/.virtualenvs/shorturl2/bin/activate "):
        c.run("git pull")
        #c.run("pip install -r requirements.txt")
        #c.run("python manage.py makemessages -a")
        c.run("python manage.py compilemessages")
        c.run("touch app/wsgi.py")

@task(hosts=["wp@url.info.gf", ])
def mydump(ctx, mysql_pass=""):
    ctx.run("mysqldump -uroot -p'{}' shorturl > dump.shorturl.sql".format(mysql_pass))
    ctx.get("dump.shorturl.sql")
    local('python manage.py dbshell < dump.shorturl.sql ')
