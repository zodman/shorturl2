# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from .models import AccountCode, Code
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.utils import timezone
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _


class CodeRedirectMixin(object):
    redir_url = "products"

    def get_context_data(self, **kwargs):
        context = super(CodeRedirectMixin, self).get_context_data(**kwargs)
        account = self.request.user.account
        now = timezone.now()
        qs = AccountCode.objects.filter(account=account, expired__gte=now)
        context["active_codes"] = qs 
        return context

    def check_user_had_code(self, request):
        account = request.user.account
        qs = AccountCode.objects.filter(account=account)
        now = timezone.now()
        qs = qs.filter(expired__gte=now)
        return qs

    def dispatch(self, request, *args, **kwargs):
        dis = super(CodeRedirectMixin, self).dispatch(request, *args, **kwargs)
        qs = self.check_user_had_code(request)
        if not qs.exists():
            messages.warning(request, _(u"Expired code"))
            return HttpResponseRedirect(reverse_lazy(self.redir_url))
        if qs.filter(code__type='free').exists():
            a = qs.first()
            messages.warning(request, 
                             _(u"Your trial expire in {}").format(a.expired))
        return dis


class ProductView(LoginRequiredMixin, TemplateView):
    template_name="codes/show_products.html"

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        account = request.user.account
        qs = AccountCode.objects.filter(account=account)
        now = timezone.now()
        qs = qs.filter(expired__lte=now)
        context["code_active"] = qs
        return self.render_to_response(context)

    def create_code(self, code):
        code = Code.objects.get(code=code.strip())
        account = self.request.user.account
        acode, created = AccountCode.objects.get_or_create(code=code, defaults=dict(account=account))
        if created:
            acode.expired = timezone.now() + timezone.timedelta(days=30)
            acode.save()
        return created

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        code = request.POST.get("code")
        try:
            created = self.create_code(code)
            if created:
                messages.info(request, _(u"Code accepted!"))
                return HttpResponseRedirect(reverse_lazy("home"))
            else:
                messages.warning(request, _(u"Code was used"))
        except (Code.DoesNotExist, ValueError):
            messages.error(request, _(u"Code not found"))
        return self.render_to_response(context)

product_view = ProductView.as_view()
