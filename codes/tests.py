from test_plus.test import TestCase
from .models import Code
import uuid

class TestCodes(TestCase):
    username ='zodman'

    def setUp(self):
        self.user = self.make_user(self.username)
    
    def test_products(self):
        with self.login(username=self.username):
            self.get_check_200("products")
            code  = Code.objects.create()
            self.post("products", data={'code': code.code})
            self.response_302() # success
            self.post("products", data={'code': code.code})
            self.response_200() # duplicated
            c = uuid.uuid4()
            self.post("products", data={'code':c})
            self.response_200() # not found
