from django.db import models
from django.utils import timezone

class AccountCodeManager(models.Manager):

    def active(self):
        now = timezone.now()
        self.filter(code__expired__lte=now)