from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from .views import *

urlpatterns = [
    url("^products/$", product_view, name="products"),
] 
