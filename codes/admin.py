from django.contrib import admin
from django.contrib import messages
from .models import Code, AccountCode
from django.utils import timezone
from import_export import resources
from import_export.admin import ImportExportModelAdmin

class CodeResource(resources.ModelResource):

    class Meta:
        model = Code
        fields = ("code",)
        exclude = ("id",)
        import_id_fields = ("code",)
        skip_unchanged = True


class CodeAdmin(ImportExportModelAdmin):
    list_display = ("code","type","exported")
    list_filter = ("type","exported")

    resource_class = CodeResource

    actions = ["mark_exported"]

    def mark_exported(self, request, queryset):
        queryset.update(exported=True)
        messages.info(request, u"Marked exported")

admin.site.register(Code, CodeAdmin)


class AccountCodeAdmin(admin.ModelAdmin):
    list_display = ("code","expired",'code_type','account', "active")
    list_filter = ("expired", "code__type")
    def active(self, obj):
        d = timezone.now()
        if obj.expired >= d.date():
            return True
        return False
    active.boolean = True
    def code_type(self, obj):
        return obj.code.type

admin.site.register(AccountCode, AccountCodeAdmin)
