from __future__ import unicode_literals
import uuid
from django.db import models
from account.models import Account
from .manager import AccountCodeManager
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.utils import timezone
CH = (
    ("free", "FREE"),
    ("prem30", "prem30"),
)

class Code(models.Model):
    code = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.CharField(max_length=10, choices=CH, default='prem30')
    exported = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    def __unicode__(self):
        return u"{}".format(self.code)

class AccountCode(models.Model):
    code = models.OneToOneField(Code)
    account = models.ForeignKey(Account)
    expired = models.DateField(null=True, blank=True, default=None)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    objects = AccountCodeManager()

@receiver(post_save, sender=Account)
def create_freemium(**kwargs):
    created = kwargs.get("created")
    if created:
        instance = kwargs.get("instance")
        code = Code.objects.create(type="free")
        expired =  timezone.now() + timezone.timedelta(days=15)
        AccountCode.objects.create(code=code, account=instance, expired=expired)

