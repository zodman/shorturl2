exports.paths = {
    public: 'media/public/',
    watched:["media/app/"]
}

exports.files = {
  javascripts: {joinTo: 'app.js'},
  stylesheets: {joinTo: 'app.css'}
};

exports.plugins = {
    browserSync: {
        files:["**/*.{scss,css,html,py,js}"],
        proxy:'localhost:8000',
        reloadDelay: 3000,
        reloadDebounced: 4000
    }
};
exports.npm = {
    static: ["node_module/famfamfam-flags/dist/sprite/famfamfam-flags.png"],
    styles: {
        'css-spaces': ["dist/spaces.min.css"],
        'famfamfam-flags': ["dist/sprite/famfamfam-flags.min.css",'dist/sprite/famfamfam-flags.png']
    }
};


