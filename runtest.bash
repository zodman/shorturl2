#python manage.py  loadtestdata core.Service:3
#python manage.py  loadtestdata core.ShortURL:10
#python manage.py  loadtestdata core.Visit:1900
set -ex
coverage run --source=. manage.py test
coverage report
coverage html
